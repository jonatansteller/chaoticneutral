<?php get_header(); ?>


<section id="content">

	<main id="primary"><?php if ( have_posts() ) : ?>

		<?php if ( is_home() && ! is_front_page() ) : ?>
			<h1 class="index-title redundant"><?php single_post_title(); ?></h1>
		<?php elseif ( is_category() ) : ?>
			<h1 class="index-title"><?php single_cat_title( '', true ); ?></h1>
			<?php the_archive_description( '<p>', '</p>' );
		elseif ( is_archive() ) :
			the_archive_title( '<h1 class="index-title">', '</h1>' );
			the_archive_description( '<p>', '</p>' );
		elseif ( is_search() ) : ?>
			<h1 class="index-title"><?php printf( __( 'Search: %s', 'chaoticneutral' ), get_search_query() ); ?></h1>
		<?php endif; ?>

		<?php while ( have_posts() ) : the_post(); ?><article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<?php if ( is_singular() ) {
				the_title( '<h1 class="post-title">', '</h1>' );
			} else {
				the_title( '<h2 class="post-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			} ?>

			<?php if ( get_post_type() == 'post' ) : ?>
				<p class="meta-top"><?php chaoticneutral_meta_top(); ?></p>
			<?php endif; ?>

			<?php $article_lists = get_theme_mod( 'article_lists' );
			if ( ! is_singular() && ( is_search() || $article_lists == 'excerpt' ) ) {
				if ( has_post_thumbnail() ) : ?>
					<div class="post-thumbnail"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'medium' ); ?></a></div>
				<?php endif;
  				the_excerpt();
			} else {
				the_content( sprintf( __( 'Continue reading <span class="redundant">"%s"</span>', 'chaoticneutral' ), the_title( '', '', false ) ) );
			} ?>

			<?php wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'chaoticneutral' ),
				'after'  => '</div>',
			)); ?>

			<p class="meta-bottom"><?php chaoticneutral_meta_bottom(); ?></p>

			<?php chaoticneutral_author_info(); ?>

		</article>

		<?php if ( is_single() ) {

			the_post_navigation( array(
				'prev_text' => '<span class="nav-subtitle">' . __( 'Previous Post', 'chaoticneutral' ) . '</span> <span class="nav-title">%title</span>',
				'next_text' => '<span class="nav-subtitle">' . __( 'Next Post', 'chaoticneutral' ) . '</span> <span class="nav-title">%title</span>',
			));

			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

		}

		endwhile;

		the_posts_navigation();

	else : ?>

		<article class="nothing">

			<h1 class="post-title"><?php _e( 'Well, this is awkward :/', 'twentyseventeen' ); ?></h1>

			<p><?php _e( 'Been trying really hard, but it looks like there is nothing at this location. Sincere apologies. Maybe you could try one of the links below or a search?', 'chaoticneutral' ); ?></p>

			<?php the_widget( 'WP_Widget_Recent_Posts' );

			the_widget( 'WP_Widget_Tag_Cloud' ); ?>

		</article>

	<?php endif; ?></main>

<?php get_sidebar(); ?>

</section>


<?php get_footer(); ?>
