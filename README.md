# Chaotic Neutral

- *Author:* Jonatan Jalle Steller
- *Requires at least:* WordPress 4.4
- *Tested up to:* WordPress 4.7
- *Version:* 0.9
- *License:* GNU General Public License v3 or later
- *License URI:* https://www.gnu.org/licenses/gpl-3.0.en.html
- *Text Domain:* chaoticneutral
- *Tags:* academic, journal, neutral, text, typography, responsive, clean

## Description

*Chaotic Neutral* is a clean, text-focused WordPress theme originally developed for an academic journal. It aims to support all WordPress standard features in a way that remains accessible to non-professionals. It was originally inspired by early stages of [TwentySeventeen](https://github.com/WordPress/twentyseventeen), but has deviated since. It is not yet fit for or available from the WordPress theme repository.

## Features

- Clean and responsive appearance, including print and smartphone layouts
- Six customisable colours
- Widget areas in the sidebar and in two columns in the footer
- Supports both custom logos and simple inline SVG logos
- Provides an option to limit the home page to recent posts from a single category
- Option to show information about the author on single pages
- Ability to replace WordPress authors through ones defined through a taxonomy

## Copyright

*Chaotic Neutral* is distributed under the terms of the GNU GPL v3.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but without any warranty of fitness for a particular purpose. See the License for more details.

The theme bundles the following third-party resources:

- *Karla font:* by Jonny Pinhorn, under the SIL Open Font License v1.1
- *Work Sans font:* by Wei Huang, under the SIL Open Font License v1.1
- *Normalize.css:* by Nicolas Gallagher and Jonathan Neal, under the MIT License

## Roadmap

Planned for version 1.0:

- Change Karla font for German ß
- Fix logo colour in print layout
- Identify a best practice for footnotes in posts

Ideas for potential improvements:

- Make fonts customisable
- Add more aria notations where applicable
- Make the theme fit for the WP theme repository
- Support custom headers through the customizer
- Support instant previews in the customizer
- Add support for infinite scrolling

## Changelog

- *Version 0.9:* initial release
