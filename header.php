<!DOCTYPE html>
<html <?php language_attributes(); ?>>


<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- Added by WordPress -->
<?php wp_head(); ?>

</head>


<body <?php if ( get_theme_mod( 'svg_logo_width') && get_theme_mod( 'svg_logo_height') && get_theme_mod( 'svg_logo_path') ) {
	$svglogo = '<a href="' . esc_url( home_url( '/' ) ) . '" rel="home"><svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 ' . get_theme_mod( 'svg_logo_width' ) . ' ' . get_theme_mod( 'svg_logo_height' ) . '" width="' . get_theme_mod( 'svg_logo_width' ) . '" height="' . get_theme_mod( 'svg_logo_height' ) . '"><path id="header-logo-svg" d="' . get_theme_mod( 'svg_logo_path' ) . '" /></svg></a>';
	body_class( 'has-svg-logo' );
} else {
	body_class();
}?>>

<header id="header">

	<a href="#content" class="redundant"><?php _e( 'Skip to content', 'chaoticneutral' ); ?></a>

	<?php if ( $svglogo ) {
		echo $svglogo;
	} elseif ( has_custom_logo() ) {
		the_custom_logo();
	} ?>

	<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>

	<?php if ( get_bloginfo('description') ) : ?>
		<p><?php bloginfo('description'); ?></p>
	<?php endif; ?>

</header>

<?php if ( has_nav_menu( 'top' ) ) : ?><nav id="navigation" role="navigation" aria-label="<?php _e( 'Main Menu', 'chaoticneutral' ); ?>">

	<?php $menu_top = wp_nav_menu( array(
		'theme_location' => 'top',
		'menu_id'        => 'top',
		'depth'          => 1,
		'container'      => '',
		'echo'           => false,
	) );
	$menu_top = str_replace("\n", "", $menu_top);
	$menu_top = str_replace("\r", "", $menu_top);
	echo $menu_top; ?>

</nav><?php endif; ?>
