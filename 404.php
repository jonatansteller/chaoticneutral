<?php get_header(); ?>


<section id="content">

	<main id="primary">

		<article class="nothing">

			<h1 class="post-title"><?php _e( 'Well, this is awkward :/', 'twentyseventeen' ); ?></h1>

			<p><?php _e( 'Been trying really hard, but it looks like there is nothing at this location. Sincere apologies. Maybe you could try one of the links below or a search?', 'chaoticneutral' ); ?></p>

			<?php the_widget( 'WP_Widget_Recent_Posts' );

			the_widget( 'WP_Widget_Tag_Cloud' ); ?>

		</article>

	</main>

<?php get_sidebar(); ?>

</section>


<?php get_footer(); ?>
