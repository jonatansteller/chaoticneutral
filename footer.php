<?php if ( has_nav_menu( 'meta' ) ) : ?><nav id="metanavigation" role="navigation" aria-label="<?php _e( 'Meta Menu', 'chaoticneutral' ); ?>">

	<?php $menu_meta = wp_nav_menu( array(
		'theme_location' => 'meta',
		'menu_class'     => 'meta',
		'depth'          => 1,
		'container'      => '',
		'echo'           => false,
	));
	$menu_meta = str_replace("\n", "", $menu_meta);
	$menu_meta = str_replace("\r", "", $menu_meta);
	echo $menu_meta; ?>

</nav><?php endif; ?>

<?php if ( is_active_sidebar( 'footer-left' ) || is_active_sidebar( 'footer-right' ) ) : ?><footer id="footer" role="complementary"><div class="widget-area">

	<?php if ( is_active_sidebar( 'footer-left' ) ) : ?><div id="footer-left" class="footer-widgets-area">
		<?php dynamic_sidebar( 'footer-left' ); ?>
	</div><?php endif; ?>

	<?php if ( is_active_sidebar( 'footer-right' ) ) : ?><div id="footer-right" class="footer-widgets-area">
		<?php dynamic_sidebar( 'footer-right' ); ?>
	</div><?php endif; ?>

</div></footer><?php endif; ?>


<?php wp_footer(); ?>


</body>


</html>
