<?php

// Theme setup
/*if ( ! function_exists( 'chaoticneutral_setup' ) ) :*/
function chaoticneutral_setup() {

	// Make theme available for translation
	load_theme_textdomain( 'chaoticneutral' );

	// Add default posts and comments RSS feed links
	add_theme_support( 'automatic-feed-links' );

	// Add WordPress-managed title tag
	add_theme_support( 'title-tag' );

	// Enable support for post thumbnails
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'chaoticneutral-featured-image', 2000, 1200, true );
	add_image_size( 'chaoticneutral-thumbnail-avatar', 100, 100, true );

	// Register two menu locations
	register_nav_menus( array(
		'top'  => __( 'Top', 'chaoticneutral' ),
		'meta' => __( 'Meta', 'chaoticneutral' ),
	));

	// Switch to HTML5 markup
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	));

	// Add theme support for custom logo
	add_theme_support( 'custom-logo', array(
		'height'      => 1000,
		'width'       => 1000,
		'flex-width'  => true,
		'flex-height' => true,
	));
}
/*endif;*/
add_action( 'after_setup_theme', 'chaoticneutral_setup' );


// Add theme customizer settings
function chaoticneutral_customize($wp_customize) {

	// Section
	$wp_customize->add_section( 'site_layout', array(
		'title'       => __( 'Site Layout', 'chaoticneutral' ),
		'priority'    => 40,
		'capability'  => 'edit_theme_options',
		'description' => __( 'Set up the general layout of the current theme.', 'chaoticneutral' )
	));
	$wp_customize->add_section( 'svg_logo', array(
		'title'       => __( 'SVG Logo', 'chaoticneutral' ),
		'priority'    => 50,
		'capability'  => 'edit_theme_options',
		'description' => __( 'If you provide an SVG logo to be used in the header, custom bitmap logos will not be used.', 'chaoticneutral' )
	));

	// Setting
	$wp_customize->add_setting( 'show_author_bio', array(
		'default'    => false,
		'type'       => 'theme_mod',
		'capability' => 'edit_theme_options',
		'transport'  => 'refresh'
	));
	$wp_customize->add_setting( 'article_lists', array(
		'default'           => 'content',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'refresh',
		'sanitize_callback' => 'sanitize_key'
	));
	$wp_customize->add_setting( 'home_category', array(
		'default'           => false,
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'refresh',
		'sanitize_callback' => 'sanitize_key'
	));
	$wp_customize->add_setting( 'color_background', array(
		'default'           => '#fff',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'refresh',
		'sanitize_callback' => 'sanitize_hex_color'
	));
	$wp_customize->add_setting( 'color_primary', array(
		'default'           => '#292c25',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'refresh',
		'sanitize_callback' => 'sanitize_hex_color'
	));
	$wp_customize->add_setting( 'color_medium', array(
		'default'           => '#8f948b',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'refresh',
		'sanitize_callback' => 'sanitize_hex_color'
	));
	$wp_customize->add_setting( 'color_light', array(
		'default'           => '#e8ebe0',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'refresh',
		'sanitize_callback' => 'sanitize_hex_color'
	));
	$wp_customize->add_setting( 'color_secondary', array(
		'default'           => '#33362e',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'refresh',
		'sanitize_callback' => 'sanitize_hex_color'
	));
	$wp_customize->add_setting( 'color_accent', array(
		'default'           => '#2196f3',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'refresh',
		'sanitize_callback' => 'sanitize_hex_color'
	));
	$wp_customize->add_setting( 'svg_logo_width', array(
		'default'           => false,
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'refresh',
		'sanitize_callback' => 'sanitize_key'
	));
	$wp_customize->add_setting( 'svg_logo_height', array(
		'default'           => false,
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'refresh',
		'sanitize_callback' => 'sanitize_key'
	));
	$wp_customize->add_setting( 'svg_logo_path', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'refresh',
		'sanitize_callback' => 'sanitize_text_field'
	));

	// Control
	$wp_customize->add_control( 'chaoticneutral_show_author_bio', array(
		'label'       => __( 'Display author information', 'chaoticneutral' ),
		'type'        => 'checkbox',
		'settings'    => 'show_author_bio',
		'section'     => 'site_layout',
		'priority'    => 10,
		'description' => __( 'Show author avatar and description below posts next to publishing date and tags', 'chaoticneutral' )
	));
	$wp_customize->add_control( 'chaoticneutral_article_lists', array(
		'label'       => __( 'Article Lists', 'chaoticneutral' ),
		'type'        => 'radio',
		'settings'    => 'article_lists',
		'section'     => 'site_layout',
		'priority'    => 15,
		'description' => __( 'Define how lists of articles such as category pages should be displayed.' ),
		'choices'     => array(
			'content' => __( 'Show all content', 'chaoticneutral' ),
			'excerpt' => __( 'Show excerpts only', 'chaoticneutral' ),
			//'boxes'   => __( 'Show excerpt boxes', 'chaoticneutral' )
		)
	));
	$wp_customize->add_control( 'chaoticneutral_home_category', array(
		'label'       => __( 'Home Category', 'chaoticneutral' ),
		'type'        => 'number',
		'settings'    => 'home_category',
		'section'     => 'site_layout',
		'priority'    => 20,
		'description' => __( 'Provide a category ID to limit the latest posts on your home page to a single category', 'chaoticneutral' )
	));
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'chaoticneutral_color_background', array(
		'label'       => __( 'Background color', 'chaoticneutral' ),
		'settings'    => 'color_background',
		'section'     => 'site_layout',
		'priority'    => 30
	)));
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'chaoticneutral_color_primary', array(
		'label'       => __( 'Primary text color', 'chaoticneutral' ),
		'settings'    => 'color_primary',
		'section'     => 'site_layout',
		'priority'    => 40
	)));
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'chaoticneutral_color_medium', array(
		'label'       => __( 'Medium text color', 'chaoticneutral' ),
		'settings'    => 'color_medium',
		'section'     => 'site_layout',
		'priority'    => 50
	)));
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'chaoticneutral_color_light', array(
		'label'       => __( 'Light text color', 'chaoticneutral' ),
		'settings'    => 'color_light',
		'section'     => 'site_layout',
		'priority'    => 60
	)));
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'chaoticneutral_color_secondary', array(
		'label'       => __( 'Header color', 'chaoticneutral' ),
		'settings'    => 'color_secondary',
		'section'     => 'site_layout',
		'priority'    => 70
	)));
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'chaoticneutral_color_accent', array(
		'label'       => __( 'Accent color', 'chaoticneutral' ),
		'settings'    => 'color_accent',
		'section'     => 'site_layout',
		'priority'    => 80
	)));
	$wp_customize->add_control( 'chaoticneutral_svg_logo_width', array(
		'label'       => __( 'Width', 'chaoticneutral' ),
		'type'        => 'number',
		'settings'    => 'svg_logo_width',
		'section'     => 'svg_logo',
		'priority'    => 10,
		'description' => __( 'Width of both viewport and viewbox', 'chaoticneutral' )
	));
	$wp_customize->add_control( 'chaoticneutral_svg_logo_height', array(
		'label'       => __( 'Height', 'chaoticneutral' ),
		'type'        => 'number',
		'settings'    => 'svg_logo_height',
		'section'     => 'svg_logo',
		'priority'    => 20,
		'description' => __( 'Height of both viewport and viewbox', 'chaoticneutral' )
	));
	$wp_customize->add_control( 'chaoticneutral_svg_logo_path', array(
		'label'       => __( 'SVG Path', 'chaoticneutral' ),
		'type'        => 'text',
		'settings'    => 'svg_logo_path',
		'section'     => 'svg_logo',
		'priority'    => 30,
		'description' => __( 'Value of the path description attribute', 'chaoticneutral' )
	));
}
add_action( 'customize_register', 'chaoticneutral_customize' );


// Restrict home page to a single category if option is set
function chaoticneutral_home( $query ) {
	$home_category_setting = get_theme_mod('home_category');
	if ( $home_category_setting && $query->is_home() && $query->is_main_query() ) {
		$query->set( 'cat', $home_category_setting );
	}
}
add_action( 'pre_get_posts', 'chaoticneutral_home' );


// Add option to define post authors through taxonomy
function chaoticneutral_register_taxonomies() {
	register_taxonomy('people', 'post', array(
		'hierarchical' => false,
		'label'        => 'Author',
		'query_var'    => true,
		'rewrite'      => true
	));
}
add_action('init', 'chaoticneutral_register_taxonomies');


// Modify feed to list authors defined through taxonomy if available
function chaoticneutral_modify_author( $name ) {
	global $post;
	$raw = get_the_term_list($post->ID, 'people', '', ', ', '');
	$authors = strip_tags($raw);
	if ( $authors ) {
		$name = $authors;
	}
	return $name;
}
add_filter( 'the_author', 'chaoticneutral_modify_author' );
add_filter( 'get_the_author_display_name', 'chaoticneutral_modify_author' );


// Set content width to 700 pixels
function chaoticneutral_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'chaoticneutral_content_width', 700 );
}
add_action( 'after_setup_theme', 'chaoticneutral_content_width', 0 );


// Register widget areas
function chaoticneutral_widgets_init() {

	// Sidebar
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'chaoticneutral' ),
		'id'            => 'sidebar',
		'description'   => __( 'Sidebar that contains secondary information.', 'chaoticneutral' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

	// Footer left
	register_sidebar( array(
		'name'          => __( 'Footer left', 'chaoticneutral' ),
		'id'            => 'footer-left',
		'description'   => __( 'Main side of the footer area.', 'chaoticneutral' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

	// Footer right
	register_sidebar( array(
		'name'          => __( 'Footer right', 'chaoticneutral' ),
		'id'            => 'footer-right',
		'description'   => __( 'Right-hand section of the footer area.', 'chaoticneutral' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
}
add_action( 'widgets_init', 'chaoticneutral_widgets_init' );


// Output custom logo
function chaoticneutral_the_custom_logo() {
	if ( function_exists( 'the_custom_logo' ) ) {
		the_custom_logo();
	}
}

// Enqueue scripts and styles
function chaoticneutral_assets() {
	wp_enqueue_style( 'chaoticneutral-style', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'chaoticneutral_assets' );


// Output meta-top byline
function chaoticneutral_meta_top() {
	$separate_meta = __( ', ', 'chaoticneutral' );

	// Author
	$author = get_the_term_list($post->ID, 'people', '<span>', ', ', '</span>');
	if ( ! $author ) {
		$author = '<span><a href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . get_the_author() . '</a></span>';
	}
	printf( _x( 'By %s', 'post author', 'chaoticneutral' ), $author );

	// Category
	$categories = get_the_category_list( $separate_meta );
	printf( '<span>' . _x( '%s', 'post categories', 'chaoticneutral' ) . ' </span>', $categories );
}


// Output meta-bottom line
function chaoticneutral_meta_bottom() {
	$separate_meta = __( ', ', 'chaoticneutral' );

	// Date
	$date_string = '<time datetime="' . get_the_date( 'c' ) . '">' . get_the_date() . '</time>';
	$date_modified = sprintf( _x( 'Last modified %s', 'date modified', 'chaoticneutral' ), get_the_modified_date() );
	printf( _x( '<span>%s </span>', 'date posted', 'chaoticneutral' ), '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="' . $date_modified . '">' . $date_string . '</a>' );

	// Tags
	if ( get_post_type() == 'post' ) {
		$tags_list = get_the_tag_list( '', $separate_meta );
		if ( $tags_list ) {
			printf( '<span>' . __( 'Tagged %s', 'chaoticneutral' ) . ' </span>', $tags_list );
		}
	}

	// Comment link
	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span>';
		comments_popup_link( __( 'Leave a comment', 'chaoticneutral' ), __( '1 comment', 'chaoticneutral' ), __( '% comments', 'chaoticneutral' ) );
		echo ' </span>';
	}

	// Edit link
	edit_post_link( sprintf( __( 'Edit<span class="redundant"> "%s"</span>', 'chaoticneutral' ), the_title( '', '', false ) ), '<span>', ' </span>' );
}


// Output author information (if option is set)
function chaoticneutral_author_info() {
	$show_author_bio = get_theme_mod( 'show_author_bio' );
	$authors = get_the_terms( $post->ID, 'people' );

	// Check if option is set and if a single post is shown
	if ( $show_author_bio && is_single() ) {

		// Use taxonomy authors, foreach in case there are multiple ones
		if( $authors ) {
			foreach( $authors as $author ) {
				chaoticneutral_author_info_echo( $author->name, $author->slug, '<img src="' . get_template_directory_uri() . '/assets/avatar.png" class="avatar avatar-100" height="100" width="100" />', $author->description );
			}

		// Use regular author as fallback
		} else {
			chaoticneutral_author_info_echo( get_the_author_meta( 'display_name' ), get_author_posts_url( get_the_author_meta( 'ID' ) ), get_avatar( get_the_author_meta('ID'), 100), get_the_author_meta( 'description' ) );
		}
	}
}

function chaoticneutral_author_info_echo( $author_name, $author_url, $author_avatar , $author_description ) {
	if ( $author_name && $author_url && $author_avatar && $author_description ) {
		echo '<p class="meta-author">';
		echo $author_avatar;
		echo '<span class="meta-author-name"><a href="' . $author_url . '">' . $author_name . '</a></span>';
		echo '<span class="redundant">: </span><span class="meta-author-description">' . $author_description . '</span>';
		echo '</p>';
	}
}


// Add placeholder text to comment form
function chaoticneutral_comment_placeholders( $fields )
{
	$req = get_option( 'require_name_email' );
	$fields['author'] = str_replace(
		'<input',
		'<input placeholder="' . __( 'Name', 'chaoticneutral' ) . ( $req ? ' *' : '' ) . '"',
		$fields['author']
	);
	$fields['email'] = str_replace(
		'<input',
		'<input placeholder="' . __( 'Email', 'chaoticneutral' ) . ( $req ? ' *' : '' ) . '"',
		$fields['email']
	);
	$fields['url'] = str_replace(
		'<input',
		'<input placeholder="' . __( 'Website', 'chaoticneutral' ) . '"',
		$fields['url']
	);
	return $fields;
}
add_filter( 'comment_form_default_fields', 'chaoticneutral_comment_placeholders' );


// Set colours provided in the customizer
function chaoticneutral_customize_css() {
	$color_background = get_theme_mod( 'color_background', '#fff' );
	$color_primary = get_theme_mod( 'color_primary', '#292c25' );
	$color_medium = get_theme_mod( 'color_medium', '#8f948b' );
	$color_light = get_theme_mod( 'color_light', '#e8ebe0' );
	$color_secondary = get_theme_mod( 'color_secondary', '#33362e' );
	$color_accent = get_theme_mod( 'color_accent', '#2196f3' );
?>
<style type="text/css">

body {
	background: <?php echo $color_background; ?>;
}

hr {
	background: <?php echo $color_medium; ?>;
}

article h1,
article h2 {
	color: <?php echo $color_primary; ?>;
}

article h3,
article h4 {
	color: <?php echo $color_medium; ?>;
}

article h5,
article h6 {
	color: <?php echo $color_light; ?>;
}

h1.index-title {
	color: <?php echo $color_medium; ?>;
}

blockquote {
	border-left-color: <?php echo $color_light; ?>;
}

pre {
	background: <?php echo $color_light; ?>;
}

abbr[title],
acronym {
	border-bottom-color: <?php echo $color_medium; ?>;
}

mark,
ins {
	background: <?php echo $color_light; ?>;
}

del,
s {
	text-decoration-color: <?php echo $color_medium; ?>;
}

a,
a:visited {
	color: <?php echo $color_accent; ?>;
}

a:hover,
a:focus,
a:active {
	color: <?php echo $color_primary; ?>;
}

#header a,
#header a:visited {
	color: <?php echo $color_background; ?>;
	fill: <?php echo $color_background; ?>;
}

#header a:hover,
#header a:focus,
#header a:active {
	color: <?php echo $color_accent; ?>;
	fill: <?php echo $color_accent; ?>;
}

#footer a:hover,
#footer a:focus,
#footer a:active {
	color: <?php echo $color_background; ?>;
}

#navigation a:hover,
#navigation a:focus,
#navigation a:active,
#metanavigation a:hover,
#metanavigation a:focus,
#metanavigation a:active {
	background: <?php echo $color_accent; ?>;
}

.meta-top a,
.meta-top a:visited,
.meta-bottom a,
.meta-bottom a:visited,
.meta-author a,
.meta-author a:visited,
.comment-meta a,
.comment-meta a:visited {
	color: <?php echo $color_medium; ?>;
}

.meta-top a:hover,
.meta-top a:focus,
.meta-top a:active,
.meta-bottom a:hover,
.meta-bottom a:focus,
.meta-bottom a:active,
.meta-author a:hover,
.meta-author a:focus,
.meta-author a:active,
.comment-meta a:hover,
.comment-meta a:focus,
.comment-meta a:active {
	color: <?php echo $color_accent; ?>;
}

input[type="text"],
input[type="email"],
input[type="url"],
input[type="password"],
input[type="search"],
input[type="number"],
input[type="tel"],
input[type="range"],
input[type="date"],
input[type="month"],
input[type="week"],
input[type="time"],
input[type="datetime"],
input[type="datetime-local"],
input[type="color"],
textarea,
select {
	color: <?php echo $color_primary; ?>;
	border-color: <?php echo $color_medium; ?>;
}

input[type="text"]:focus,
input[type="email"]:focus,
input[type="url"]:focus,
input[type="password"]:focus,
input[type="search"]:focus,
input[type="number"]:focus,
input[type="tel"]:focus,
input[type="range"]:focus,
input[type="date"]:focus,
input[type="month"]:focus,
input[type="week"]:focus,
input[type="time"]:focus,
input[type="datetime"]:focus,
input[type="datetime-local"]:focus,
input[type="color"]:focus,
textarea:focus,
select:focus {
	color: <?php echo $color_accent; ?>;
	border-color: <?php echo $color_primary; ?>;
}

input[type="text"]::placeholder,
input[type="email"]::placeholder,
input[type="url"]::placeholder,
input[type="password"]::placeholder,
input[type="search"]::placeholder,
input[type="number"]::placeholder,
input[type="tel"]::placeholder,
input[type="range"]::placeholder,
input[type="date"]::placeholder,
input[type="month"]::placeholder,
input[type="week"]::placeholder,
input[type="time"]::placeholder,
input[type="datetime"]::placeholder,
input[type="datetime-local"]::placeholder,
input[type="color"]::placeholder,
textarea::placeholder {
	color: <?php echo $color_medium ?>;
}

button,
input[type="button"],
input[type="reset"],
input[type="submit"] {
	background: <?php echo $color_primary; ?>;
	color: <?php echo $color_background; ?>;
}

button:hover,
button:focus,
input[type="button"]:hover,
input[type="button"]:focus,
input[type="reset"]:hover,
input[type="reset"]:focus,
input[type="submit"]:hover,
input[type="submit"]:focus {
	background: <?php echo $color_accent; ?>;
}

td,
th {
	border-color: <?php echo $color_medium; ?>;
}

#header {
	background: <?php echo $color_secondary; ?>;
	color: <?php echo $color_background; ?>;
}

#navigation,
#metanavigation {
	background: <?php echo $color_primary; ?>;
	color: <?php echo $color_background; ?>;
}

#navigation a,
#navigation a:visited,
#metanavigation a,
#metanavigation a:visited {
	color: <?php echo $color_background; ?>;
}

#footer {
	background: <?php echo $color_secondary; ?>;
	color: <?php echo $color_background; ?>;
}

.meta-top,
.meta-bottom,
.meta-author {
	color: <?php echo $color_medium; ?>;
}

.comment-navigation .nav-links,
.post-navigation .nav-links,
.posts-navigation .nav-links {
	border-bottom-color: <?php echo $color_medium; ?>;
	border-top-color: <?php echo $color_medium; ?>;
}

.comment-list {
	border-bottom-color: <?php echo $color_medium; ?>;
}

.comment-body {
	border-top-color: <?php echo $color_medium; ?>;
}

.comment-meta,
.comment-list .reply {
	color: <?php echo $color_medium; ?>;
}

.widget ul li {
	border-bottom-color: <?php echo $color_medium; ?>;
	border-top-color: <?php echo $color_medium; ?>;
}

#footer .widget ul li {
	border-bottom-color: <?php echo $color_medium; ?>;
	border-top-color: <?php echo $color_medium; ?>;
}

.widget_tag_cloud a {
	border-color: <?php echo $color_medium; ?>;
}

#footer .widget_tag_cloud a {
	border-color: <?php echo $color_medium; ?>;
}

.widget_tag_cloud a:hover,
.widget_tag_cloud a:focus {
	border-color: <?php echo $color_primary; ?>;
}

@media screen and (max-width: 950px) {

	#secondary {
		background: <?php echo $color_light; ?>;
	}

}

</style>
<?php }
add_action( 'wp_head', 'chaoticneutral_customize_css' );

?>
