<?php /* Template Name: List Categories */ get_header(); ?>


<section id="content">

	<main id="primary"><?php if ( have_posts() ) : ?>

		<?php while ( have_posts() ) : the_post(); ?><article class="issues" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<?php the_title( '<h1 class="post-title">', '</h1>' ); ?>

			<?php the_content( sprintf( __( 'Continue reading <span class="redundant">"%s"</span>', 'chaoticneutral' ), the_title( '', '', false ) ) ); ?>

			<?php wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'chaoticneutral' ),
				'after'  => '</div>',
			)); ?>

			<?php the_widget( 'WP_Widget_Categories' ); ?>

		</article>

		<?php endwhile;

	endif; ?></main>

<?php get_sidebar(); ?>

</section>


<?php get_footer(); ?>
