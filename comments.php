<?php if ( post_password_required() ) {
	return;
} ?>

<section id="comments" class="comments-area">

	<?php if ( have_comments() ) : ?>

		<h2 class="comments-title"><?php printf( _nx( 'One Comment', '%s Comments', get_comments_number(), 'comments title', 'chaoticneutral' ), number_format_i18n( get_comments_number() ) ); ?></h2>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?><nav id="comment-nav-above" class="navigation comment-navigation" role="navigation">
			<ul class="nav-links">
				<li class="nav-previous"><?php previous_comments_link( __( 'Older Comments', 'chaoticneutral' ) ); ?></li>
				<li class="nav-next"><?php next_comments_link( __( 'Newer Comments', 'chaoticneutral' ) ); ?></li>
			</ul>
		</nav><?php endif; ?>

		<ol class="comment-list">
			<?php wp_list_comments( array(
				'avatar_size' => 100,
				'style'      => 'ol',
				'short_ping' => true,
			)); ?>
		</ol>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?><nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
			<ul class="nav-links">
				<li class="nav-previous"><?php previous_comments_link( __( 'Older Comments', 'chaoticneutral' ) ); ?></li>
				<li class="nav-next"><?php next_comments_link( __( 'Newer Comments', 'chaoticneutral' ) ); ?></li>
			</ul>
		</nav><?php endif; ?>

	<?php endif; ?>

	<?php if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) {
		echo '<p>' . __( 'Comments are closed.', 'chaoticneutral' ) . '</p>';
	} ?>

	<?php comment_form(); ?>

</section>
